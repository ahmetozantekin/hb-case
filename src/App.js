import React from "react";
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ListPage from './Pages/ListPage';
import AddPage from './Pages/AddPage';
import Nav from './Components/Nav';
import { ToastContainer  } from 'react-toastify';
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div className="container">
      <div className="App">
        <Router>
          <Nav />
          <Switch>
            <Route path="/addlink" component={AddPage} />
            <Route path="/add" component={AddPage} />
            <Route path="/" component={ListPage} />
            <Route component={ListPage} />
          </Switch>
        </Router>
      </div>

      <ToastContainer
        position="top-center"
        autoClose={5000}
        hideProgressBar
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
}

export default App;
