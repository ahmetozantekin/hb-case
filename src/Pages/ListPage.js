import React, { useState } from "react";
import { connect } from "react-redux";
import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content'
import * as TodoActions from "../Redux/modules/todos";
import { bindActionCreators } from "redux";
import SubmitLinkButton from '../Components/SubmitLinkButton';
import ListItem from '../Components/ListItem';
import { toast } from 'react-toastify';


function ListPage({ todos, todoActions }) {
    const MySwal = withReactContent(Swal)
    const [sortValue, setSortValue] = useState("most");
    const flattenTodos = React.useMemo(() => { return Object.values(todos) }, [todos]);
    const toastConfig = {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    }

    const upVote = (todo) => {
        todoActions.upVote(todo.id)
    }

    const downVote = (todo) => {
        todoActions.downVote(todo.id)
    }

    const remove = (todo) => {
        MySwal.fire({
            title: "Do you want to remove",
            text: todo.text,
            showDenyButton: true,
            denyButtonText: "Cancel",
            showCloseButton: true
        }).then((result) => {
            if (result.value) {
                todoActions.remove(todo.id);
                toast.success(`${todo.text} removed`, toastConfig);
            } else {
                toast.error(`Canceled for remove ${todo.text}`, toastConfig);
            }
        })
    }

    const handleSortType = (e) => {
        setSortValue(e.target.value);
    }
    
    return (
        <>
            <div className="list wrapper">
                <SubmitLinkButton />
                <select  value={sortValue} onChange={handleSortType}>
                    <option value="most">Most Voted (Z - A)</option>
                    <option value="less">Less Voted (A - Z)</option>
                </select>
                {
                    sortValue === "most" ? 
                    flattenTodos
                        .sort((a, b) => {
                            return b.score - a.score;
                        })
                        .map(todo => {
                            return <ListItem
                                todo={todo}
                                key={todo.id}
                                upVote={upVote}
                                downVote={downVote}
                                remove={remove} />
                        })
                    :
                    flattenTodos
                        .sort((a, b) => {
                            return a.score - b.score;
                        })
                        .map(todo => {
                            return <ListItem
                                todo={todo}
                                key={todo.id}
                                upVote={upVote}
                                downVote={downVote}
                                remove={remove} />
                        })
                }
            </div>

        </>
    );
}

const mapStateToProps = (state) => {
    return { todos: state.todos }
};
const mapDispatchToProps = (dispatch) => {
    return {
        todoActions: bindActionCreators({ ...TodoActions }, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ListPage);