import React, { useState } from "react";
import { connect } from "react-redux";
import * as TodoActions  from "../Redux/modules/todos";
import { bindActionCreators } from "redux";
import { Link } from "react-router-dom";
import { toast } from 'react-toastify';

function AddPage({actions}) {
    const [url, setUrl] = useState();
    const [text, setText] = useState();
    const toastConfig = {
        position: "top-center",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    }

    const add = () => {
        if(url===undefined || text===undefined) {
            toast.error(`Link Name & Link URL must be a value`, toastConfig);
        } else {
            actions.add({url, text, score :0 }) // default score value = 0
            toast.success(`${text} added`, toastConfig);
        }
    }

    return (
        <>
            <div className="add wrapper">
                <Link to="/">
                    ← Return to List
                </Link>
                <h1>Add New Link</h1>

                <div className="add-control">
                    <label>Link Name</label>
                    <input value={text} onChange={(e)=>setText(e.target.value)}/>
                </div>
                <div className="add-control">
                    <label>Link URL</label>
                    <input value={url} onChange={(e)=>setUrl(e.target.value)}/>
                </div>

                <button onClick={add}>Add</button>
            </div>
        </>
    );
}


const mapStateToProps = (state) => {
    return { }
};

const mapDispatchToProps = (dispatch) => {
    return { 
        actions: bindActionCreators({...TodoActions}, dispatch)
    }
};

export default connect(mapStateToProps,mapDispatchToProps)(AddPage);