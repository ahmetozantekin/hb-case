import uuid from "react-uuid";
import update from "immutability-helper";
/** 
 * Redux Ducks 
 */

// Types
const ADD_TODO = 'ADD_TODO';
const UPVOTE = 'UPVOTE';
const DOWNVOTE = 'DOWNVOTE';
const REMOVE_TODO = 'REMOVE_TODO';

const initialState = {};

// Reducer
export default function reducer(state = initialState, action = {}) {
    switch (action.type) {
        case ADD_TODO:
            let id = uuid();
            return {
                ...state, 
                [id]: {...action.todo, id} 
            };
        case UPVOTE:
            return update(state, { [action.id]: { score: { $apply: x => x + 1 } } });
            
        case DOWNVOTE:
            return update(state, { [action.id]: { score: { $apply: x => x - 1 } } });
            
        case REMOVE_TODO:
            const {[action.id]:actionId, ...others} = state;
            return others;

        default:
            return state;
    }
}

// Action Creator
export function add(todo) {
    return {
        type: ADD_TODO,
        todo
    };
}

export function upVote(id) {
    return {
        type: UPVOTE,
        id
    };
}

export function downVote(id) {
    return {
        type: DOWNVOTE,
        id
    };
}

export function remove(id) {
    return {
        type: REMOVE_TODO,
        id
    };
}