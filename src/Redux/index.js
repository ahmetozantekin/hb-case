import {createStore} from 'redux'
import { persistStore, persistCombineReducers } from 'redux-persist'
import storage from 'redux-persist/es/storage' // default: localStorage if web, AsyncStorage if react-native
import rootReducer from './modules'

/** Localstorage configs */
const config = {
   key: 'root',
   storage,
};

const reducer = persistCombineReducers(config, rootReducer);

export default function configureStore () {
   let store = createStore(reducer);
   let persistor = persistStore(store);

   return { persistor, store }
}