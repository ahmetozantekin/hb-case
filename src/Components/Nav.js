import React from "react";
import { Link } from "react-router-dom";

function Nav() {
    return (
        <header>
            <Link to="/"><div className="brand"></div></Link>
            <div className="logo"><b>Link</b><span>VOTE</span> Challange</div>
        </header>
    );
}

export default Nav;
