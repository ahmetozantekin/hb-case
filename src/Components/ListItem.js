import React from "react";

function ListItem({todo, upVote, downVote, remove}) {
    return (
        <div className="list-item">
            <div className="delete" onClick={()=> {remove(todo)}}>
                <svg xmlns="http://www.w3.org/2000/svg"  x="0px" y="0px" viewBox="0 0 410.42 410.42"> <path d="M205.21,0C92.057,0,0,92.057,0,205.21s92.057,205.21,205.21,205.21s205.21-92.057,205.21-205.21S318.364,0,205.21,0z   M331.21,229.631c0,5.5-4.5,10-10,10h-232c-5.5,0-10-4.5-10-10V180.79c0-5.5,4.5-10,10-10h232c5.5,0,10,4.5,10,10V229.631z"/> </svg>
            </div>
            <div className="point">
                {todo.score || 0}
                <span>POINTS</span>
            </div>
            <div className="body">
                <h2>{todo.text}</h2>
                <a href={todo.url} target="_blank">({todo.url})</a>

                <div className="votes">
                    <div className="upvote" onClick={()=> {upVote(todo)}}>
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512.005 512.005" > <g> <path d="M466.22,205.787L263.553,3.12c-4.16-4.16-10.923-4.16-15.083,0L45.804,205.787c-21.803,21.803-21.803,57.28,0,79.083    s57.28,21.803,79.083,0l77.781-77.781v251.584c0,29.397,23.936,53.333,53.333,53.333s53.333-23.936,53.333-53.333V207.088    l77.781,77.781c21.803,21.803,57.28,21.803,79.083,0C488.001,263.088,488.001,227.589,466.22,205.787z" /> </g> </svg>
                        Up Vote
                    </div>
                    <div className="downvote" onClick={()=> {downVote(todo)}}>
                        <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 512 512"> <g> <path d="M466.216,227.115c-21.803-21.803-57.28-21.803-79.083,0l-77.803,77.803V53.333C309.331,23.936,285.395,0,255.997,0    s-53.333,23.936-53.333,53.333v251.584l-77.781-77.781c-21.803-21.803-57.28-21.803-79.083,0s-21.803,57.28,0,79.083    l202.667,202.667c2.069,2.069,4.8,3.115,7.531,3.115c2.731,0,5.461-1.045,7.552-3.115l202.667-202.667    C487.997,284.416,487.997,248.917,466.216,227.115z" /> </g> </svg>
                        Down Vote
                    </div>

                </div>
            </div>
        </div>
    );
}

export default ListItem;
