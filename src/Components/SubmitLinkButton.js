import React from "react";
import { Link } from "react-router-dom";

function SubmitLinkButton() {
    return (
        <Link to="/add" className="submit-btn">
            <div className="submit">
                <div className="icon">
                    <svg width="448pt" viewBox="0 0 448 448" height="448pt" xmlns="http://www.w3.org/2000/svg"><path d="m272 184c-4.417969 0-8-3.582031-8-8v-176h-80v176c0 4.417969-3.582031 8-8 8h-176v80h176c4.417969 0 8 3.582031 8 8v176h80v-176c0-4.417969 3.582031-8 8-8h176v-80zm0 0"/></svg>
                </div>

                SUBMIT A LINK
            </div>
        </Link>
    );
}

export default SubmitLinkButton;
